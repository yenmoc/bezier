﻿using System;
using System.Collections.Generic;
using UnityEngine;

// ReSharper disable UnusedMember.Global
// ReSharper disable MemberCanBeProtected.Global
// ReSharper disable SuggestVarOrType_SimpleTypes
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable once CheckNamespace
namespace UnityModule.BezierCurves
{
    /// <summary>
    /// A bezier curve of arbitrary degree using a Bernstein Polynominal.
    /// Based on this code...
    /// https://www.codeproject.com/Articles/25237/Bezier-Curves-Made-Simple
    /// </summary>
    public class Bezier2 : Bezier
    {
        /// <summary>
        /// The curves degree. 1 is linear, 2 quadratic, etc.
        /// </summary>
        public int ControlLength => Control.Length - 1;

        /// <summary>
        /// The control points.
        /// </summary>
        public Vector2[] Control { get; set; }

        public Bezier2(EBezier type) : this((int) type)
        {
        }

        public Bezier2(int numberControl)
        {
            if (numberControl > MAX_CONTROL || numberControl < MIN_CONTROL)
                throw new ArgumentException($"Number Control can not be greater than {MAX_CONTROL} or less than {MIN_CONTROL}.");

            Control = new Vector2[numberControl + 1];
        }

        public Bezier2(EBezier type, IList<Vector2> control)
        {
            var number = (int) type;
            var p = number + 1;
            if (number > MAX_CONTROL || number < MIN_CONTROL)
                throw new ArgumentException($"Number control can not be greater than {MAX_CONTROL} or less than {MIN_CONTROL}.");

            if (control.Count > p || control.Count < p)
                throw new ArgumentException($"Number element of Control need equals to {p}");

            Control = new Vector2[control.Count];
            for (int i = 0; i < control.Count; i++)
            {
                Control[i] = control[i];
            }
        }

        public Bezier2(IList<Vector2> control)
        {
            int numberControl = control.Count - 1;
            if (numberControl > MAX_CONTROL || numberControl < MIN_CONTROL)
                throw new ArgumentException($"Number control can not be greater than {MAX_CONTROL} or less than {MIN_CONTROL}.");

            Control = new Vector2[control.Count];
            for (int i = 0; i < control.Count; i++)
                Control[i] = control[i];
        }

        /// <summary>
        /// The position on the curve at t.
        /// </summary>
        /// <param name="t">Number between 0 and 1.</param>
        public Vector2 Position(float t)
        {
            if (t < 0) t = 0;
            if (t > 1) t = 1;
            Vector2 p = new Vector2();
            for (int i = 0; i < Control.Length; i++)
            {
                float basis = Bernstein(ControlLength, i, t);
                p += basis * Control[i];
            }

            return p;
        }

        /// <summary>
        /// The tangent on the curve at t.
        /// </summary>
        /// <param name="t">Number between 0 and 1.</param>
        public Vector2 Tangent(float t)
        {
            Vector2 d = FirstDerivative(t);
            d.Normalize();
            return d;
        }

        /// <summary>
        /// The normal on the curve at t.
        /// </summary>
        /// <param name="t">Number between 0 and 1.</param>
        public Vector2 Normal(float t)
        {
            Vector2 d = FirstDerivative(t);
            d.Normalize();
            return new Vector2(d.y, -d.x);
        }

        /// <summary>
        /// The first derivative on the curve at t.
        /// </summary>
        /// <param name="t">Number between 0 and 1.</param>
        public Vector2 FirstDerivative(float t)
        {
            if (t < 0) t = 0;
            if (t > 1) t = 1;

            float inv = 1.0f / ControlLength;
            Vector2 d = new Vector2();

            for (int i = 0; i < Control.Length - 1; i++)
            {
                float basis = Bernstein(ControlLength - 1, i, t);
                d += basis * inv * (Control[i + 1] - Control[i]);
            }

            return d * 4.0f;
        }

        /// <summary>
        /// Fills the array with positions on the curve.
        /// </summary>
        public void GetPositions(IList<Vector2> points)
        {
            int count = points.Count;
            float t = 0;
            float step = 1.0f / (count - 1.0f);

            for (int i = 0; i < count; i++)
            {
                for (int j = 0; j < Control.Length; j++)
                {
                    float basis = Bernstein(ControlLength, j, t);
                    points[i] += basis * Control[j];
                }

                t += step;
            }
        }

        /// <summary>
        /// Arc length of curve via intergration.
        /// </summary>
        public float Length(int steps, float tmax = 1.0f)
        {
            if (tmax <= 0) return 0;
            if (tmax > 1) tmax = 1;

            if (ControlLength == 1)
                return Vector2.Distance(Control[0], Control[1]) * tmax;
            else
            {
                steps = Math.Max(steps, 2);
                float len = 0;
                Vector2 previous = Position(0);

                for (int i = 1; i < steps; i++)
                {
                    float t = i / (steps - 1.0f) * tmax;
                    Vector2 p = Position(t);

                    len += Vector2.Distance(previous, p);
                    previous = p;
                }

                return len;
            }
        }

        /// <summary>
        /// Returns the position at t using DeCasteljau's algorithm.
        /// Same as Position(t) but slower.
        /// </summary>
        public Vector2 DeCasteljau(float t)
        {
            int count = Control.Length;
            Vector2[] q = new Vector2[count];
            Array.Copy(Control, q, count);

            for (int k = 1; k < count; k++)
            {
                for (int i = 0; i < count - k; i++)
                    q[i] = (1.0f - t) * q[i] + t * q[i + 1];
            }

            return q[0];
        }

        /// <summary>
        /// Splits the bezier at t and returns the two curves.
        /// </summary>
        /// <param name="t">Position to split (0 to 1).</param>
        /// <param name="b0">The curve from 0 to t.</param>
        /// <param name="b1">The curve from t to 1.</param>
        public void Split(float t, out Bezier2 b0, out Bezier2 b1)
        {
            int count = Control.Length;
            Vector2[] q = new Vector2[count];
            Array.Copy(Control, q, count);

            b0 = new Bezier2(ControlLength);
            b1 = new Bezier2(ControlLength);

            b0.Control[0] = Control[0];
            b1.Control[count - 1] = Control[count - 1];

            for (int k = 1; k < count; k++)
            {
                int len = count - k;
                for (int i = 0; i < len; i++)
                    q[i] = (1.0f - t) * q[i] + t * q[i + 1];

                b0.Control[k] = q[0];
                b1.Control[len - 1] = q[len - 1];
            }
        }
    }
}